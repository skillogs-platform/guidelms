# Sommaire

## Guide d'utilisateur

**Connexion**
- [Première connexion à la plateforme](./connexion/README.md)

**Réinitialisation**
- [Réinitialisation de son mot de passe](./reinitialisation/password/README.md)
- [Réinitialisation d'un mot de passe d'un compte](./reinitialisation/account/README.md)

**Compte**
- [Création d'un compte](./account/README.md)

**Promotion**
- [Création d'une promotion](./promotion/create/README.md)
- [Gérer les apprenant.e.s d'une promotion](./promotion/student/README.md)

## Site internet

- [Skillogs](https://skillogs.com)
- [Guide Acarya](http://guide-acarya.skillogs.com)
