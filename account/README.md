# Création de compte

Vous êtes connecté.e à la plateforme `LMS`.

![dashboard](../commun/dashboard.png)

Cliquez sur l'onglet **Utilisateur.rice.s** (`encadré rouge`).

![utilisateur](../commun/dashboardUser.png)

Une fois sur cette page, cliquez sur le bouton **Nouvel.le utilisateur.rice** (`indication 2`).

![utilisateur](user.png)

Une fois sur la nouvelle page, vous pouvez remplir le formulaire :
- mail (`indication 1`)
- prénom (`indication 2`)
- nom (`indication 3`)
- date de naissance (`indication 4`)

![form](userForm.png)

Ensuite, vous devez définir quel est le rôle du compte qui est créé :
- Administrateur
- Etudiant
- Ingénieur de formation
- Professeur
- Tuteur

**ATTENTION** ce rôle définit l'accès de l'utilisateur à la plateforme `LMS` ou `Acarya`, ainsi que ses possibilités sur chacune d'entre-elles.

![role](userRole.png)

Puis, vous devez définir la langue dans laquelle la plateforme s'affichera.

Le **français** est choisi par défaut.

![langue](userLangue.png)

Confirmez la création de l'utilisateur.rice en cliquant sur le bouton **Créer un.e utilisateur.rice** (`encadré rouge`).

![save](userSave.png)

Un écran confirmera la création de l'utilisateur.rice.

![saved](userSaved.png)

Vous pouvez enchaîner la création d'utilisateur.rice en cliquant sur le lien **Créer un.e nouvel.le utilisateur.rice** (`encadré rouge`).

![next](nextUser.png)