# Création de compte

Vous êtes connecté.e à la plateforme `LMS`.

![dashboard](../../commun/dashboard.png)

Cliquez sur l'onglet **Promotions** (`encadré rouge`).

![utilisateur](../../commun/dashboardPromo.png)

Une fois sur la page Promotions, vous pouvez :

- rechercher une promotion en filtrant par nom (`indication 1`)
- créer une promotion en cliquant sur le bouton **Nouvelle promotion** (`indication 2`)

![filter](promoFiltered.png)

- créer une promotion après un filtre sans résultat en cliquant sur le bouton **Créer une promotion** (`encadré rouge`)

![create](promoCreate.png)

Une fois sur la page de création d'une promotion, vous devez remplir le formulaire avec les éléments suivants :
- Le nom de la promotion (`indication 1`)
- L'ingénieur de formation en charge de la promotion (`indication 2`)
- Le cursus que suit la promotion (`indication 3`)
- Une date de début (`indication 4`)
- Une date de fin (`indication 5`)

Ensuite vous devez cliquer sur le bouton **Créer une promotion** (`encadré rouge`).

![form](promoForm.png)

La promotion est créée, le bandeau d'information vous propose de :
- consulter les **informations générales** (`indication 1`)
- consulter les **apprenant.e(s)** rattachés à la promotions (`indication 2`)
- **supprimer la promotion** (`indication 3`)

![created](promoCreated.png)

Ou vous pouvez revenir sur la page listant toutes les promotions en cliquant sur le lien **retour** (`encadré rouge`).

![retour](promoBack.png)

![list](promoList.png)