# Gérer les apprenant.e.s d'une promotion

Vous êtes connecté.e à la plateforme `LMS`.

![dashboard](../../commun/dashboard.png)

Cliquez sur l'onglet **Promotions** (`encadré rouge`).

![utilisateur](../../commun/dashboardPromo.png)

Une fois sur la page Promotions, vous pouvez rechercher la promotion en filtrant par nom (`indication 1`), puis cliquer sur son nom dans la liste de résultat (`indication 2`).

![filter](promoFiltered.png)

Vous êtes dirigé vers la page d'information de la promotion.

![info](promoInfo.png)

Vous pouvez cliquer sur le bouton **Actions** (`indication 1`) afin de déplier un menu, puis cliquer sur le lien **Gérer les apprenant.e.s** (`indication 2`).

![action](promoAction.png)

Vous vous rendez sur la page de gestion des apprenant.e.s de la promotion.

![students](promoStudents.png)

Vous pouvez rechercher un.e apprenant.e par nom, prénom ou email dans le champ de recherche (`indication 1`).  
Les résultats s'affichent en dessous, et une fois l'apprenant.e trouvé.e vous pouvez l'ajouter à la promotion, en cliquant sur le lien **Ajouter**.

![search](promoStudentsSearch.png)

L'apprenant.e est ajouté.e à la liste de la promotion, et vous pouvez constater combien sont intégré.e.s à la pormotion (`encadrés rouge`).  
Vous pouvez ajouter d'autres apprenant.e.s à la liste de la même manière avant de cliquer sur le bouton **Sauvegarder** (`indication 1`).

![save](promoStudentsSave.png)

En cas d'erreur, vous pouvez retirer un.e apprenant.e de la liste, en cliquant sur l'**icône corbeille** (`encadré rouge`), puis cliquer sur le bouton **Sauvegarder** afin d'enregistrer vos modifications (`encadré 1`).

![delete](promoStudentsDelete.png)

Vous pouvez revenir à la page de la promotion en cliquant sur le lien **Retour** (`encadré rouge`).

![back](promoStudentsBack.png)

Vous êtes redirigé vers la page de la promotion avec le.s apprenant.e.s ajouté.e.s ou retiré.e.s.

![list](promoStudentsList.png)