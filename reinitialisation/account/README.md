# Réinitialisation d'un mot de passe d'un compte

Vous êtes connecté.e à la plateforme `LMS`.

![dashboard](../../commun/dashboard.png)

Cliquez sur l'onglet **Utilisateur.rice.s** (`encadré rouge`).

![utilisateur](../../commun/dashboardUser.png)

Depuis la page **Utilisateur.rice.s", vous pouvez effectuer une recherche sur le nom, prénom, ou email dans la barre de recherche (`indicateur 1`).

Une fois le compte recherché dans la liste, vous pouvez cliquer dessus (`indicateur 2`).

![search](userSearch.png)

Vous êtes redirigé sur la page du compte de l'utilisateur.rice.

![account](accountUser.png)

Vous pouvez ensuite cliquer sur le lien **Réinitialiser le mot de passe** (`encadré rouge`).

![reset](accountReset.png)

Vous devez confirmer en cliquant sur le lien **Etes vous sûr ?** (`encadré rouge`).

![sure](accountSure.png)

Un message de confirmation apparaît.

![reinit](accountReinit.png)

L'utilisateur.rice a reçu un mail de réinitialisation de son mot de passe.
