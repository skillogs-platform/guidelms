# Réinitialisation de votre mot de passe

Lorsque vous vous trouvez sur la page de connexion, vous pouvez cliquer sur le lien **Mot de passe oublié ?**

![accueil](accueil.png)

Vous êtes redirigé vers une page sur laquelle vous devez :
 - renseigner votre email (`indication 1`)
 - cliquer sur le bouton **Envoyer** (`indication 2`)

![reinitialisation](reinitialisation.png)

 Vous serez informé de la confirmation de l'envoi du mail à l'adresse renseignée :

 ![envoi](envoiMail.png)

Vous recevrez un mail de réinitialisation du mot de passe.

**`ATTENTION`** ce mail peut arriver dans vos SPAMS :

![mail](mailReinit.png)

Afin de réinitialiser votre mot de passe vous pouvez cliquer soit :
- sur le bouton **REINITIALISER LE MOT DE PASSE** (`indication 1`)
- sur le lien (`indication 2`)

![mail](mailReinitIndic.png)

Vous êtes redirigé vers la page suivante :

![mot de passe](../../commun/mdpForm.png)

Dans le formulaire il faut :

- renseigner votre nouveau `mot de passe` (`indication 1`)
- cliquer sur le bouton **Sauvegarder** (`indication 2`)

![mot de passe](../../commun/mdpFormRempli.png)

Une nouvelle page vous confirmera la modification de votre mot de passe.

![reset](../../commun/mdpChange.png)

Cliquer ensuite sur le lien **Retour à la page de connexion**.

![retour](../../commun/retourConnexion.png)

Vous êtes redirigé sur la page de connexion ce qui vous permet de vous identifier et d'accéder au **`LMS de Skillogs`**.

![connexion](../../commun/connexion.png)